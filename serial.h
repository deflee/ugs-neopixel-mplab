/* 
 * File:   serial.h
 * Author: wklee
 *
 * Created on January 5, 2021, 11:00 AM
 */

#ifndef SERIAL_H
#define	SERIAL_H

void serial_init(void);
bool serial_getPacket(uint8_t* data, uint8_t* len);
void serial_sendData(uint8_t flag, uint8_t* data, uint8_t len);

#endif	/* SERIAL_H */

