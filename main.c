/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.6
        Device            :  PIC18F57Q43
        Driver Version    :  2.00
 */

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
 */

#include "mcc_generated_files/mcc.h"
#include "serial.h"

#define TOTAL_NO_OF_LED 1024
#define RUNNING_NO_OF_LED 10

#define SHOW_BUFFER_ELEMENT 64
#define SHOW_BUFFER_SIZE    SHOW_BUFFER_ELEMENT*24

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} Pixel_t;

Pixel_t Pixels[TOTAL_NO_OF_LED];
uint8_t showBuffer[SHOW_BUFFER_SIZE + 1];
uint8_t showBuffer2[SHOW_BUFFER_SIZE + 1];

void dmaResetState(void);
void dma1_InterruptHandler(void);
void tmr4_InterruptHandler(void);
void changeBrightnesstoArray(uint8_t green, uint8_t red, uint8_t blue);
void dmaSend(void);
void changeBrightnesstoArrayV2(uint8_t green, uint8_t red, uint8_t blue);
void changeBrightnesstoArrayV3(uint8_t green, uint8_t red, uint8_t blue);
void changeBrightnesstoArrayV4(uint8_t green, uint8_t red, uint8_t blue);
void TMR4InterruptHandler(void);
void patternGenerator(void);
void running(void);
void led_ON(uint8_t green, uint8_t red, uint8_t blue);
void led_OFF(void);

enum {
    TESTING,
    ON_COLOR,
    SENDLOW
};
uint8_t State = SENDLOW;

uint16_t globalBrightness = 0;

/*
                         Main application
 */
uint8_t buffer[25] = {25, 50, 25, 50, 25, 50, 25, 50,
    25, 50, 25, 50, 25, 50, 25, 50,
    25, 50, 25, 50, 25, 50, 25, 50, 0};

//reset for 62.5us
uint8_t reset[50] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

uint32_t dmaLEDCounter = 0;
uint8_t button = 0;
uint16_t current_LED = 100;
uint8_t total_no_of_bit = TOTAL_NO_OF_LED * 3;
bool dmaSendFlag = true; //to turn off DMA
bool dmaFinishSend1BitFlag = true; //to know 1 bit is finish sending
uint16_t effStep = 0;
uint8_t TMR4_Counter = 0;
uint8_t runningCount = 0;

bool patternStepUpdate = false;
bool blinkUpdate = false;
uint16_t b = 0;
bool lastShow = false;
bool showStart = false;

uint8_t currentMode = 3;    //0:0ff, 1:0n, 2: blink, 3: fade
uint8_t currentRed,currentGreen,currentBlue;
uint8_t currentCounter = 0;
uint8_t currentBrightness = 0;
uint16_t currentBlinkPeriod = 200;
uint16_t currentBlinkCounter = 0;
uint8_t currentFadePeriod = 5;
uint8_t currentFadeCounter = 0;
uint8_t currentFadeDirection = 0;
uint8_t currentRed = 100;
uint8_t currentGreen = 100;
uint8_t currentBlue = 100;

uint16_t hbCounter = 0;

void delay(uint32_t i) {
    while (i--);
}

/**
 End of File
 */
void dma1_InterruptHandler(void) {
    //DMA1_StopTransfer();
    //DMAnCON0bits.SIRQEN = 0;

    //DMAnCON0 = 0x80;
    //TMR2_StopTimer();
    //PWM1_LoadDutyValue(0);
    //dmaSend();
    //dmaSend();
    //DMAnCON0bits.EN = 0;
    PIR3bits.TMR2IF = 0;
    dmaSendFlag = true;
    PWM1_LoadDutyValue(0);
    //PWM1_LoadDutyValue(0);

}

void dmaSendShow(uint8_t* ptr, uint16_t size) {

    //DMA1_Initialize();
    DMAnCON0bits.EN = 0;
    //    DMA1_Initialize();
    //DMA1_SetSCNTIInterruptHandler(dma1_InterruptHandler);

    DMASELECT = 0x00;
    dmaSendFlag = false;
    
    DMAnSSA = (uint24_t) ptr;
    DMAnSSZ = size;
    DMAnCON1bits.SMR  = 0;
    DMAnDSA = (uint16_t) & CCPR1L;
    DMAnDSZ= 1;
    DMAnSIRQ = 0x1B;
    DMAnCON1bits.SSTP = 1;
    DMAnCON1bits.DSTP = 0;
    DMAnCON0bits.EN = 1;
    DMAnCON0bits.SIRQEN = 1;
    
    
//    DMA1_SetSourceAddress((uint24_t) ptr);
    DMA1_SetSourceSize(size);
//
//    DMA1_SelectSourceRegion(0x00); // 00 = GPR, 01 = program flash
//    DMA1_SetDestinationAddress((uint16_t) & CCPR1L); //CCP 1 address
//    DMA1_SetDestinationSize(1); //destination size bits
//    DMA1_SetStartTrigger(0x1B); //Timer 2 as start trigger
//
//    DMAnCON1bits.SSTP = 1;
//    DMAnCON1bits.DSTP = 0;
//
//    DMAnCON0bits.EN = 1;
//
//
//    //    DMA1_StartTransfer();
//    DMA1_StartTransferWithTrigger();

}

void dmaSendShow2(uint8_t *ptr) {
    while (dmaSendFlag == false);



}

void changeBrightnesstoArrayV5(uint8_t green, uint8_t red, uint8_t blue, uint8_t* buf) {
    uint8_t bits[8] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
    uint8_t i = 0;

    for (i = 0; i < 8; i++) {
        if (green & bits[i])
            buf[i] = 50;
        else
            buf[i] = 25;
    }

    for (i = 0; i < 8; i++) {
        if (red & bits[i])
            buf[i + 8] = 50;
        else
            buf[i + 8] = 25;
    }

    for (i = 0; i < 8; i++) {
        if (blue & bits[i])
            buf[i + 16] = 50;
        else
            buf[i + 16] = 25;
    }

}

void setLEDColor(uint16_t i, uint8_t red, uint8_t green, uint8_t blue) {
    Pixels[i].red = red;
    Pixels[i].green = green;
    Pixels[i].blue = blue;

}

void patternGenerator(void) {
    float factor1, factor2;
    uint16_t ind;


    for (uint16_t j = 0; j < TOTAL_NO_OF_LED; j++) {

        ind = 60 - (uint16_t) (effStep - j * 1) % 60;
        switch ((int) ((ind % 60) / 20)) {
            case 0: factor1 = 1.0 - ((float) (ind % 60 - 0 * 20) / 20);
                factor2 = (float) ((int) (ind - 0) % 60) / 20;
                setLEDColor(j, 255 * factor1 + 0 * factor2, 0 * factor1 + 255 * factor2, 0 * factor1 + 0 * factor2);
                //changeBrightnesstoArrayV4( 255 * factor1 + 0 * factor2, 0 * factor1 + 255 * factor2, 0 * factor1 + 0 * factor2);
                //dmaSend();
                break;
            case 1: factor1 = 1.0 - ((float) (ind % 60 - 1 * 20) / 20);
                factor2 = (float) ((int) (ind - 20) % 60) / 20;
                setLEDColor(j, 0 * factor1 + 0 * factor2, 255 * factor1 + 0 * factor2, 0 * factor1 + 255 * factor2);
                //                changeBrightnesstoArrayV4( 0 * factor1 + 0 * factor2, 255 * factor1 + 0 * factor2, 0 * factor1 + 255 * factor2);
                //                dmaSend();
                break;
            case 2: factor1 = 1.0 - ((float) (ind % 60 - 2 * 20) / 20);
                factor2 = (float) ((int) (ind - 40) % 60) / 20;
                setLEDColor(j, 0 * factor1 + 255 * factor2, 0 * factor1 + 0 * factor2, 255 * factor1 + 0 * factor2);
                //                changeBrightnesstoArrayV4( 0 * factor1 + 255 * factor2, 0 * factor1 + 0 * factor2, 255 * factor1 + 0 * factor2);
                //                dmaSend();
                break;
        }


    }
    if (effStep >= 60) {
        effStep = 0;
    } else
        effStep++;
}

uint8_t pattern2() {
    // Strip ID: 0 - Effect: Fade - LEDS: 60
    // Steps: 8.9375 - Delay: 32
    // Colors: 2 (0.0.0, 255.255.255)
    // Options: duration=286, every=1, 

    uint16_t duration = 5000;
    uint16_t steps = 20; //ms
    uint16_t effSteps = 5000 / 20;
    uint8_t r, g, b;
    double e;
    e = (effStep * steps) / (double) duration; //20ms rate, full 1000ms, step = 1000/20 = 50
    r = (e) * 255 + 0 * (1.0 - e);
    g = (e) * 255 + 0 * (1.0 - e);
    b = (e) * 255 + 0 * (1.0 - e);
    for (uint16_t j = 0; j < TOTAL_NO_OF_LED; j++) {
        if ((j % 1) == 0)
            setLEDColor(j, r, g, b);
        else
            setLEDColor(j, 0, 0, 0);
    }
    if (effStep >= effSteps)
        effStep = 0;
    else
        effStep++;
    return 0x01;
}

uint8_t pattern3() {
    // Strip ID: 0 - Effect: Move - LEDS: 60
    // Steps: 60 - Delay: 50
    // Colors: 0 ()
    // Options: toLeft=true, rotate=true, 

    uint16_t effSteps = 100;
    uint8_t r, g, b;
    if (effStep == 0) {
        setLEDColor(0, 0, 0, 85);
        for (uint16_t j = 1; j < (TOTAL_NO_OF_LED); j++)
            setLEDColor(j, 0, 0, 0);
    } else {
        r = Pixels[TOTAL_NO_OF_LED - 1].red; //shift the pixel to left in the buffer
        g = Pixels[TOTAL_NO_OF_LED - 1].green;
        b = Pixels[TOTAL_NO_OF_LED - 1].blue;

        for (int16_t j = (TOTAL_NO_OF_LED - 1); j > 0; j--)
            setLEDColor(j, Pixels[j - 1].red, Pixels[j - 1].green, Pixels[j - 1].blue);

        setLEDColor(0, r, g, b);

    }

    if (effStep >= TOTAL_NO_OF_LED) //when finish move one time
        effStep = 0;
    else effStep++;
    return 0x01;
}

void outputPWM_1() {
    while (PIR3bits.TMR2IF == 0);
    PIR3bits.TMR2IF = 0;
    PWM1_LoadDutyValue(50);
}

void outputPWM_0() {
    while (PIR3bits.TMR2IF == 0);
    PIR3bits.TMR2IF = 0;
    PWM1_LoadDutyValue(25);
}

void directShow(uint8_t green, uint8_t red, uint8_t blue) {
    uint8_t bits[8] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
    uint8_t i = 0;

    for (i = 0; i < 8; i++) {
        if (green & bits[i])
            outputPWM_1();
        else
            outputPWM_0();
    }

    for (i = 0; i < 8; i++) {
        if (red & bits[i])
            outputPWM_1();
        else
            outputPWM_0();

    }

    for (i = 0; i < 8; i++) {
        if (blue & bits[i])
            outputPWM_1();
        else
            outputPWM_0();

    }
}

void show(void) { //send the led data to the neopixel
    uint16_t totalLed = TOTAL_NO_OF_LED;
    uint16_t i = 0;
    uint8_t j;
    uint8_t* ptr;
    uint8_t b = 0;
    uint16_t len;
    uint16_t c;
    ptr = &showBuffer;

    for (i = 0; i < SHOW_BUFFER_SIZE; i++) {
        showBuffer[i] = 0;
        showBuffer2[i] = 0;
    }

    //    PWM1_LoadDutyValue(0);
    //    CCP1CONbits.EN = 1;
    //    PIR3bits.TMR2IF = 0;
    //    TMR2_WriteTimer(0);
    //    TMR2_StartTimer();
    //        
    //    for (i = 0; i < TOTAL_NO_OF_LED; i++) {
    //        directShow(Pixels[i].green, Pixels[i].red, Pixels[i].blue);
    //    }
    //
    //    CCP1CONbits.EN = 0;
    //    LATBbits.LATB5 = 0;

    ptr = &showBuffer;
    for (j = 0; j < SHOW_BUFFER_ELEMENT; j++) {
            //                changeBrightnesstoArrayV5(Pixels[i + j].green, Pixels[i + j].red, Pixels[i + j].blue, ptr);
            changeBrightnesstoArrayV5(0, 0, 10, ptr);
            ptr += 24;
        }
    

    showStart = false;

    i = 0;
    lastShow = false;
    while (i < TOTAL_NO_OF_LED) {
//        if (b) {
//          
//            ptr = &showBuffer;
//        } else {
//           
//            ptr = &showBuffer2;
//        }



//        for (j = 0; j < SHOW_BUFFER_ELEMENT; j++) {
//            //                changeBrightnesstoArrayV5(Pixels[i + j].green, Pixels[i + j].red, Pixels[i + j].blue, ptr);
//            changeBrightnesstoArrayV5(0, 0, 10, ptr);
//            ptr += 24;
//        }

        i += SHOW_BUFFER_ELEMENT;
        if (i >= TOTAL_NO_OF_LED) {
            len = SHOW_BUFFER_SIZE + 1;
            showBuffer[SHOW_BUFFER_SIZE] = 0;
        } else {
            len = SHOW_BUFFER_SIZE;
        }

        if (showStart)
            if (DMAnCON0bits.SIRQEN)
                while (DMAnCON0bits.SIRQEN);

        //dmaSendShow(&showBuffer, len);
        DMAnCON0bits.EN = 0;
    //    DMA1_Initialize();
    //DMA1_SetSCNTIInterruptHandler(dma1_InterruptHandler);

        DMASELECT = 0x00;
        dmaSendFlag = false;

        DMAnSSA = (uint24_t) showBuffer;
        DMAnSSZ = len;
        DMAnCON1bits.SMR  = 0;
        DMAnDSA = (uint16_t) & CCPR1L;
        DMAnDSZ= 1;
        DMAnSIRQ = 0x1B;
        DMAnCON1bits.SSTP = 1;
        DMAnCON1bits.DSTP = 0;
        DMAnCON0bits.EN = 1;
        DMAnCON0bits.SIRQEN = 1;
//        if (b) {
//
//            dmaSendShow(&showBuffer, len);
//        } else
//            dmaSendShow(&showBuffer2, len);

        showStart = true;

//        if (b)
//            b = 0;
//        else
//            b = 1;
    }

    while (DMAnCON0bits.SIRQEN);
    DMAnCON0bits.EN = 0;
    // PWM1_LoadDutyValue(0);

}

void TMR4InterruptHandler(void) {
    TMR4_Counter++;
    if (TMR4_Counter >= 4) {
        TMR4_Counter = 0;
        
        
        if(currentMode == 0 || currentMode == 1)
        {
            currentCounter++;
            if(currentCounter >= 100)
            {
                currentCounter = 0;
                blinkUpdate = true;
            }
        }
        else if(currentMode == 2)   // slow blink
        {
            currentBlinkCounter++;
            blinkUpdate = true;
            if(currentBlinkCounter >= currentBlinkPeriod)
            {
                //blinkUpdate = true;
                currentBlinkCounter = 0;
                if(globalBrightness != 0)
                    globalBrightness = 0;
                else
                    globalBrightness = 255;
            }
            
        }
        else if(currentMode == 3)
        {
            currentFadeCounter++;
            blinkUpdate = true;
            if(currentFadeCounter >= currentFadePeriod)
            {
                //blinkUpdate = true;
                currentFadeCounter = 0;
                if(currentFadeDirection == 0)
                {
                    if (globalBrightness < 255)
                        globalBrightness++;
                    else
                    {
                        currentFadeDirection = 1;
                    }
                }
                else
                {
                    if (globalBrightness > 0)
                        globalBrightness--;
                    else
                    {
                        currentFadeDirection = 0;
                    }
                }
            }
        }
        
        

    }
    
    hbCounter++;
    if(hbCounter >= 400)
    {
        hbCounter= 0;
        IO_RC7_Toggle();
    }


}

void showDirect(uint8_t red, uint8_t green, uint8_t blue, uint16_t brightness)
{
    uint8_t real_red, real_green, real_blue;
    uint16_t ledCount = 0;
    
    real_red = (uint32_t) brightness * red / 255;
    real_green = (uint32_t)  brightness *  green / 255;
    real_blue = (uint32_t)  brightness *  blue / 255;
    
    changeBrightnesstoArrayV5(real_green, real_red, real_blue, showBuffer);
    
    
        DMASELECT = 0x00;
        dmaSendFlag = false;

        DMAnSSA = (uint24_t) &showBuffer;
        DMAnSSZ = 24;
        DMAnCON1bits.SMR  = 0;
        DMAnDSA = (uint16_t) & CCPR1L;
        DMAnDSZ= 1;
        DMAnSIRQ = 0x1B;
        DMAnCON1bits.SSTP = 0;
        DMAnCON1bits.DSTP = 0;
        DMAnCON0bits.EN = 1;
        DMAnCON0bits.SIRQEN = 1;
        PIR2bits.DMA1SCNTIF = 0;
    
    while(ledCount < TOTAL_NO_OF_LED)
    {
        while(PIR2bits.DMA1SCNTIF == 0)
            DMASELECT = 0x00;
        PIR2bits.DMA1SCNTIF = 0;
        ledCount++;
    }
    DMAnCON0bits.EN = 0;
    PWM1_LoadDutyValue(0);
}


void commandParser(void)
{
    uint8_t rxData[32];
    uint8_t rxLen = 0;
    
    if(serial_getPacket(rxData, &rxLen))
    {
        if(rxData[0] == 0x84)
        {
            serial_sendData(0x01,NULL,0);
            switch(rxData[1])
            {
                case 0:
                {
                    currentMode = 0;
                    globalBrightness = 0;
                    blinkUpdate = true;
                    break;
                }
                case 1:
                {
                    currentMode = 1;
                    currentRed = rxData[2];
                    currentGreen = rxData[3];
                    currentBlue = rxData[4];
                    globalBrightness = 255;
                    blinkUpdate = true;
                    break;
                }
                    
                    
                case 2: //blink slow mode 1s
                {
                    currentMode = 2;
                    currentBlinkPeriod = 250;
                    currentRed = rxData[2];
                    currentGreen = rxData[3];
                    currentBlue = rxData[4];
                    globalBrightness = 255;
                    currentBlinkCounter = 0;
                    blinkUpdate = true;
                }
                    break;

                   
                    
                case 3: //blink normal mode 0.5s
                {
                    currentMode = 2;
                    currentBlinkPeriod = 125;
                    currentRed = rxData[2];
                    currentGreen = rxData[3];
                    currentBlue = rxData[4];
                    globalBrightness = 255;
                    currentBlinkCounter = 0;
                    blinkUpdate = true;
                }
                    break;
              
                case 4: //blink fast mode 0.15s
                {
                    currentMode = 2;
                    currentBlinkPeriod = 37;
                    currentRed = rxData[2];
                    currentGreen = rxData[3];
                    currentBlue = rxData[4];
                    globalBrightness = 255;
                    currentBlinkCounter = 0;
                    blinkUpdate = true;
                }
                    break;
                    
                case 5: //fade slow mode (3s from dim to light up)
                {
                    currentMode = 3;
                    currentFadePeriod = 3;
                    currentRed = rxData[2];
                    currentGreen = rxData[3];
                    currentBlue = rxData[4];
                    currentFadeCounter = 0;
                    globalBrightness = 0;
                    blinkUpdate = true;
                    break;
                }
                    
                    
                case 6: //fade normal mode (2s from dim to light up)
                {
                    currentMode = 3;
                    currentFadePeriod = 2;
                    currentRed = rxData[2];
                    currentGreen = rxData[3];
                    currentBlue = rxData[4];
                    currentFadeCounter = 0;
                    globalBrightness = 0;
                    blinkUpdate = true;
                    break;
                }
                    
                    
                case 7: //fade fast mode (1s from dim to light up)
                {
                    currentMode = 3;
                    currentFadePeriod = 1;
                    currentRed = rxData[2];
                    currentGreen = rxData[3];
                    currentBlue = rxData[4];
                    currentFadeCounter = 0;
                    globalBrightness = 0;
                    blinkUpdate = true;
                    break;
                }
                    
                    
            }
        }
    }
    
}

void main(void) {
    // Initialize the device
    uint16_t i;
    uint8_t c = 0;

    
    
    INTERRUPT_GlobalInterruptEnable();
    SYSTEM_Initialize();
    
    
    TMR2_Stop();
    //DMA1_SetSCNTIInterruptHandler(dma1_InterruptHandler);
    TMR4_SetInterruptHandler(TMR4InterruptHandler);
    TMR4_StopTimer();
    //Configuration

    for (i = 0; i < SHOW_BUFFER_SIZE; i++) {
        showBuffer[i] = 0;
        showBuffer2[i] = 0;
    }

    //T4PR = 50;
    TMR4_StartTimer();
    CCP1CONbits.EN = 0;
    LATBbits.LATB5 = 0;
    DMAnCON0bits.EN = 1;

    CCP1CONbits.EN = 1;
    PWM1_LoadDutyValue(0);
    // TMR2_WriteTimer(0);
    TMR2_StartTimer();

    serial_init();

    while (1) {
        if (blinkUpdate) {
            blinkUpdate = false;

            showDirect(currentRed,currentGreen,currentBlue,globalBrightness);
        }

        if (IO_RC4_GetValue() == 0) {
            if(c == 0)
                showDirect(200,0,0,100);
            else if(c == 1)
                showDirect(0,200,0,100);
            else if(c == 2)
                showDirect(0,0,200,100);
            while (IO_RC4_GetValue() == 0);
            c++;
            if(c > 2)
                c = 0;
        }
        
        
        
        commandParser();

    }
}