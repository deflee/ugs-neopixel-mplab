
#include "mcc_generated_files/mcc.h"

uint8_t receiveBuffer[32];
uint8_t receiveLen = 0;
bool receiveStarted = false;

uint8_t sendBuffer[32];
uint8_t sendLen = 0;

uint8_t txSeq = 0;
uint8_t rxSeq = 0;


void serial_init(void)
{
    UART1_Initialize();
    
    
            
    
}


void serial_startReceive(void)
{
    if(!receiveStarted)
    {
        uint8_t i;
        for(i = 0; i< 32; i++)
            receiveBuffer[i] = 0;

        DMASELECT = 0x01;
        DMAnCON0bits.EN = 0;
        
        DMAnSSA = (uint24_t) &U1RXB;
        DMAnSSZ = 1;
        DMAnSCNT = 0;
        
        DMAnDSA = (uint16_t) receiveBuffer;
        DMAnDSZ= 32;
        DMAnDCNT = 0;
        
        DMAnSIRQ = 0x20;        // U1RX trigger

        
        
        DMAnCON1bits.DMODE  = 1;    // destination pointer increase
        DMAnCON1bits.SMODE  = 0;    // source pointer increase
        DMAnCON1bits.SMR  = 0;
        DMAnCON1bits.SSTP = 0;
        DMAnCON1bits.DSTP = 1;
        DMAnCON0bits.SIRQEN = 1;
        //DMAnCON0bits.DGO = 1;
        U1CON0bits.RXEN = 1;
        
        DMAnCON0bits.EN = 1;
        
        receiveStarted = true;
    }
}

void serial_stopReceive(void)
{
    DMAnCON0bits.EN = 0;
    receiveStarted = false;
    U1CON0bits.RXEN = 0;
}

bool serial_getPacket(uint8_t* data, uint8_t* len)
{
    uint8_t rxLen = 0;
    uint8_t i, cs;
    serial_startReceive();
            
    DMASELECT = 0x01;
    if(DMAnDCNT < 32 )
    {
        if(receiveBuffer[0] != 0x5a)
        {
            receiveStarted = false;
            serial_stopReceive();
            return false;
        }
         
        else if(DMAnDCNT <= 28)
        {
            rxLen = receiveBuffer[3];
            if(DMAnDCNT <= (27-rxLen))
            {
                cs = 0;
                for(i = 0; i < (rxLen+5); i++)
                {
                    cs ^= receiveBuffer[i];
                }
                if(cs == 0)
                {
                    for(i = 0; i< rxLen; i++)
                        data[i] = receiveBuffer[4+i];
                    
                    *len = rxLen;
                    receiveStarted = false;
                    serial_stopReceive();
                    return true;
                }
                else
                {
                    receiveStarted = false;
                     serial_stopReceive();
                    return false;
                }
            }
        }
    }
//    else if(receiveBuffer[0] != 0x5a)
//    {
//        receiveStarted = false;
//        return false;
//    }
    
    return false;
}

void serial_sendData(uint8_t flag, uint8_t* data, uint8_t len)
{
    uint8_t i;
    uint8_t cs;
    
    sendBuffer[0] = 0x5A;
    sendBuffer[1] = flag;
    sendBuffer[2] = txSeq;
    sendBuffer[3] = len;
    for(i = 0; i< len; i++)
        sendBuffer[i+4] = data[i];
    
    cs = 0;
    for(i = 0; i< (len+4); i++)
        cs ^= sendBuffer[i];
    
    sendBuffer[len+4] = cs;
    
    
    DMASELECT = 0x02;
    DMAnCON0bits.EN = 0;

    DMAnSSA = (uint24_t) sendBuffer;
    DMAnSSZ = len +5;
    DMAnSCNT = 0;

    DMAnDSA = (uint16_t) &U1TXB;
    DMAnDSZ= 1;
    DMAnDCNT = 0;

    DMAnSIRQ = 0x21;        // U1TX trigger



    DMAnCON1bits.DMODE  = 0;    // destination pointer increase
    DMAnCON1bits.SMODE  = 1;    // source pointer increase
    DMAnCON1bits.SMR  = 0;
    DMAnCON1bits.SSTP = 1;
    DMAnCON1bits.DSTP = 0;
    DMAnCON0bits.SIRQEN = 1;
    //DMAnCON0bits.DGO = 1;
    DMAnCON0bits.EN = 1;
}